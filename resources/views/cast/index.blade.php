@extends('layout.master')

@section('judul')
Halaman Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-success mb-3">Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col">Action</th>
      
      </tr>
    </thead>
    <tbody>
            @forelse ($cast as $key=>$item)
                <tr>
                    <td>{{$key +1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->umur}}</td>
                    <td>{{$item->bio}}</td>
                
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @method('delete')
                        @csrf
                            <input type="submit" class="btn btn-danger btn-sm" value="delete">
                        
                    </form>
                </td>
                </tr>
            @empty
            <tr>

            <td>data masih kosong</td>
            </tr>

            

            @endforelse
    </tbody>
  </table>

  @endsection