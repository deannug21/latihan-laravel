@extends('Layout.master')

@section('judul')
Halaman edit cast {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>nama cast</label>
      <input type="text" name="nama" value="{{$cast->nama}}"class="form-control">
    </div>

    @error('nama cast')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>umur</label>
      <input type="text" name="umur" class="form-control">{{$cast->umur}}
    
    </div>

    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>bio</label>
      <textarea name="bio" class="form-control" cols="8" rows="10">{{$cast->bio}}</textarea>

    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection