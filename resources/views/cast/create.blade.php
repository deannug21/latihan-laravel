@extends('layout.master')

@section('judul')
Cast
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>nama cast</label>
      <input type="text" name="nama" class="form-control">
    </div>

    @error('nama cast')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>umur</label>
      <input type="text" name="umur" class="form-control">
    
    </div>

    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>bio</label>
      <textarea name="bio" class="form-control" cols="8" rows="10"></textarea>

    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection